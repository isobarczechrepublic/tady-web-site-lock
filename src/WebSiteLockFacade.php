<?php
namespace BistroAgency\WebSiteLock;

use Illuminate\Support\Facades\Facade;

class WebSiteLockFacade extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'websitelock';
	}
}
