<?php

namespace BistroAgency\WebSiteLock;

use Closure;
use Illuminate\Http\Request;

class WebSiteLock
{
	/**
	 * Handle an incoming request.
	 *
	 * @param Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next)
	{
		if (!config('websitelock.enabled')) {
			return $next($request);
		}

		$hashedPassword = $this->getHashedPassword(config('websitelock.password'));

		if (session()->has('WebSiteLock') && session('WebSiteLock') === $hashedPassword) {
			return $next($request);
		}

		if (
			$request->header('Website-Lock-Token') === $hashedPassword
			|| $request->query('websitelocktoken') === $hashedPassword
			|| $request->cookie('websitelocktoken') === $hashedPassword
		) {
			return $next($request);
		}

		$url = $request->fullUrl();
		return redirect()->route('webSiteLock.index')->with('url', $url);
	}

	/**
	 * Returns the hash of a password. Implementation can change in the future.
	 *
	 * @param string $password
	 * @return string
	 */
	public static function getHashedPassword($password)
	{
		return md5($password);
	}
}
