<?php

//website lock
Route::group(['middleware' => ['web'], 'prefix' => 'web-site-lock'], function () {
	Route::get('/', [
		'uses' => 'BistroAgency\WebSiteLock\WebSiteLockController@index',
		'as' => 'webSiteLock.index'
	]);

	Route::post('/', [
		'uses' => 'BistroAgency\WebSiteLock\WebSiteLockController@checkPassword',
		'as' => 'webSiteLock.checkPassword'
	]);

	Route::get('/token/validate', [
		'uses' => 'BistroAgency\WebSiteLock\WebSiteLockController@validateToken',
		'as' => 'webSiteLock.validateToken'
	]);

	Route::get('/token', [
		'uses' => 'BistroAgency\WebSiteLock\WebSiteLockController@acquireToken',
		'as' => 'webSiteLock.acquireToken'
	]);
});
