<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
		* {
			-webkit-box-sizing: border-box;
			box-sizing: border-box;
		}

		body {
			padding: 15px;
			margin: 0;
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			-ms-flex-align: center;
			align-items: center;
			-webkit-box-pack: center;
			-ms-flex-pack: center;
			justify-content: center;
			-webkit-box-orient: vertical;
			-webkit-box-direction: normal;
			-ms-flex-direction: column;
			flex-direction: column;
			font-family: Helvetica, Arial, sans-serif;
		}

		body, html {
			background-color: #f74902;
			width: 100%;
			height: 100%;
		}

		.alert__wrap {
			width: 100%;
			position: absolute;
			bottom: 100%;
			margin-bottom: 10px;
		}

		.alert {
			background: #da0000;
			border-radius: 3px;
			color: #fff;
			font-size: 14px;
			padding: 10px;
			margin: 0;
		}

		.alert + .alert {
			margin-top: 5px;
		}

		.login {
			position: relative;
			width: 100%;
			max-width: 320px;
		}

		.login__heading {
			font-size: 22px;
			color: #ffffff;
			margin-top: 0;
			margin-bottom: 1em;
			font-weight: 500;
		}

		.login__form {
			margin-bottom: 26px;
		}

		.login__form-label {
			display: none;
		}

		.login__form-input-wrap {
			display: -webkit-box;
			display: -ms-flexbox;
			display: flex;
		}

		.login__form-password {
			background-color: #eaeaff;
			-moz-appearance: none;
			-webkit-appearance: none;
			padding: 5px 10px;
			margin: 0;
			height: 36px;
			font-size: 14px;
			border: 0;
			border-radius: 0;
			border-top-left-radius: 3px;
			border-bottom-left-radius: 3px;
			min-width: 0;
			-webkit-box-flex: 1;
			-ms-flex: 1 1 0px;
			flex: 1 1 0;
			-webkit-transition: background-color 200ms ease;
			-o-transition: background-color 200ms ease;
			transition: background-color 200ms ease;
		}

		.login__form-password:focus, .login__form-password:hover {
			outline: 0;
			background-color: #ffffff;
		}

		.login__form-submit {
			-moz-appearance: none;
			-webkit-appearance: none;
			border: 0;
			border-radius: 0;
			border-top-right-radius: 3px;
			border-bottom-right-radius: 3px;
			background-color: #232323;
			color: #ffffff;
			margin: 0;
			padding: 10px;
			height: 36px;
			-webkit-transition: background-color 200ms ease;
			-o-transition: background-color 200ms ease;
			transition: background-color 200ms ease;
			font-size: 14px;
			-webkit-box-flex: 0;
			-ms-flex: 0 0 auto;
			flex: 0 0 auto;
		}

		.login__form-submit:focus, .login__form-submit:hover {
			outline: 0;
			cursor: pointer;
			background: #101010;
		}
	</style>
</head>

<body>
<div class="login">
	@if (Session::has('errors-custom'))
		<div class="alert__wrap">
			@foreach (Session::get('errors-custom') as $error)
				<p class="alert" role="alert">
					<i class="fa fa-exclamation" aria-hidden="true"></i> {{ $error }}
				</p>
			@endforeach
		</div>
	@endif

	<h1 class="login__heading">This website is locked.</h1>

	<form class="login__form" method="post" action="{{ route('webSiteLock.checkPassword') }}">
		{{ csrf_field() }}

		<input type="hidden" name="url" value="{{ session('url') !== null ? session('url') : old('url') }}">

		<div class="login__form-input-wrap">
			<label for="login-password" class="login__form-label">Password</label>

			<input
				id="login-password"
				class="login__form-password"
				type="password"
				name="password"
				placeholder="Insert password..."
				autofocus
			>

			<input class="login__form-submit" type="submit" value="Unlock">
		</div>
	</form>
</div>
</body>

</html>
