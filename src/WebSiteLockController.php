<?php

namespace BistroAgency\WebSiteLock;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Routing\Controller as BaseController;

class WebSiteLockController extends BaseController
{
	public function __construct()
	{
		$this->middleware('web');
	}

	/**
	 * Get page with login input
	 *
	 * @return mixed
	 */
	public function index()
	{
		return view('web-site-lock::index');
	}

	/**
	 * Validate entered password
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function checkPassword(Request $request)
	{
		$enteredPassword = WebSiteLock::getHashedPassword($request->input('password'));
		$hashedPassword = WebSiteLock::getHashedPassword(config('websitelock.password'));

		if ($enteredPassword !== $hashedPassword) {
			push_error('Wrong password');
			return back()->withInput();
		}

		session()->put('WebSiteLock', $enteredPassword);

		if ($request->input('url') === '' || $request->input('url') === null) {
			return redirect('/');
		}

		return redirect($request->input('url'));
	}

	/**
	 * Use password to acquire access token
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function acquireToken(Request $request)
	{
		$enteredPassword = WebSiteLock::getHashedPassword($request->input('password'));
		$hashedPassword = WebSiteLock::getHashedPassword(config('websitelock.password'));

		if ($enteredPassword !== $hashedPassword) {
			sleep(1);
			return response()->json([
				'token' => null,
				'message' => 'Wrong password.'
			], 401);
		}

		return response()->json([
			'token' => $enteredPassword,
			'message' => 'OK'
		], 200);
	}

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function validateToken(Request $request)
	{
		$token = $request->header('Website-Lock-Token');
		$hashedPassword = WebSiteLock::getHashedPassword(config('websitelock.password'));

		if ($hashedPassword !== $token) {
			return response()->json([
				'message' => 'Invalid website lock token supplied',
			], 403);
		}

		return response()->json([
			'message' => 'OK'
		], 200);
	}
}
