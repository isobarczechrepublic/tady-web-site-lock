<?php

return [

	/*
	|--------------------------------------------------------------------------
	| WebSiteLock
	|--------------------------------------------------------------------------
	| All following variables may be also defined in .env
	| The env definition override values of config settings.
	|
	*/

	'password' => env('WEB_SITE_LOCK_PASSWORD', 'bistro'),
	'enabled' => env('WEB_SITE_LOCK_ENABLED', false),

];
