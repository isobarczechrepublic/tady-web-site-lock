<?php

namespace BistroAgency\WebSiteLock;

use Illuminate\Support\ServiceProvider;

class WebSiteLockServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadViewsFrom(__DIR__.'/views', 'web-site-lock');

		$this->publishes([
			__DIR__ . '/config' => config_path(),
		], 'web-site-lock-configs');
		$this->publishes([
			__DIR__.'/views' => resource_path('views/vendor/web-site-lock'),
		], 'web-site-lock-views');

		require __DIR__ . '/routes.php';
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->mergeConfigFrom(
			__DIR__ . '/config/websitelock.php', 'websitelock'
		);

		$this->app['router']->aliasMiddleware('WebSiteLock', WebSiteLock::class);
	}
}
