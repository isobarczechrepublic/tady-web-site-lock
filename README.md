# tady-web-site-lock

## How to add this package to app
Edit composer.json, add following to root section:

    ...
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/bistroagency/tady-web-site-lock.git"
        }
    ],
    ...
    

Edit composer.json, add following to "require" section:

    "bistroagency/tady-web-site-lock": "dev-master"

Composer.json now looks like:

    ...
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/bistroagency/tady-web-site-lock.git"
        }
    ],
    "require": {
        "php": ">=5.6.4",
        "barryvdh/laravel-debugbar": "^2.3",
        "barryvdh/laravel-ide-helper": "^2.2",
        "doctrine/dbal": "^2.5",
        "intervention/image": "^2.3",
        "jenssegers/model": "^1.1",
        "laravel/framework": "5.4.*",
        "laravel/tinker": "~1.0",
        "modelizer/selenium": "^1.2",
        "bistroagency/tady-web-site-lock": "dev-master"
    },
    ...

Run composer

    composer update

If it is the first time, when you accessing bistroagency private repository, you will hit a dialog to add consumer 
credentials. Then see **How to set up server access to the repository of this package** and then return back.

Update cofig/app.php, add a Service Provider

    // BistroAgency packages
    BistroAgency\WebSiteLock\WebSiteLockServiceProvider::class

Publish configs

    php artisan vendor:publish --tag=web-site-lock-configs

Publish views if needed (modification)

    php artisan vendor:publish --tag=web-site-lock-views

Both previous publishes accepts option --force in case you need overwrite already published items


Add to .env

    WEB_SITE_LOCK_ENABLED=true
    WEB_SITE_LOCK_PASSWORD=password

Add WebSiteLock middleware to routes you need to lock. For example in file routes/web.php:

    Route::group(['namespace' => 'Front', 'middleware' => ['WebSiteLock']], function () {
        ...
    }

Or you can apply middleware to whole route map in app/Providers/RouteServiceProvider.php like this:

    Route::middleware('web', 'WebSiteLock')
        ->namespace($this->namespace)
        ->group(base_path('routes/web.php'));

**Don't forget to add from the package published files into Bitbucket repository!** 

## How to set up server access to the repository of this package

It's necessary ro run composer manually through ssh terminal, when you add a bistroagency package to composer for the 
first time. It's because a Bitbucket's OAuth token, witch must be stored in ~/.composer/auth.json to be used by composer
to access private repository.
If you run composer with private repository dependency for the first time, you are going to be asked for Consumer Key 
and Consumer Secret. If you don't have any yet you should receive some from Bitbucket.org. Follow the instructions on 
[https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html](https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html). 
Ensure you enter a "Callback URL" (http://example.com is fine) or it will not be possible to create an Access Token (this callback url will not be used 
by composer). Composer will use stored credentials for next time automatically.

#Acquiring an access token

You can bypass the Website Lock password screen by providing an access token in the URL query string.
To acquire a token, make a `GET` request to:

	/web-site-lock/token?password={PASSWORD}
	
Example request URL and response:

```
www.example.com/web-site-lock/token?password=bistro
```
```
{
	"token": "a013e2f989067f94f083706f96cbe1c3",
	"message": "OK"
}
```

Use the acquired token like this:

	www.example.com?websitelocktoken=a013e2f989067f94f083706f96cbe1c3

You can also specify the token as a cookie, but you have to make an exception in `app/Http/Middleware/EncryptCookies.php`:

	protected $except = [
		'websitelocktoken'
	];

## TODO
* push_error()
* composer update bistroagency/tady-web-site-lock
* php artisan vendor:publish --provider="BistroAgency\WebSiteLock\WebSiteLockProvider"
* Would be nice have some variable scenario defined in config what to do with authorized user from BistroPass at 
src/routes.php#52:55

